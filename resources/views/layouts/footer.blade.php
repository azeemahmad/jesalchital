<footer class="footer footer-padding">
    <div class="container">
        <div class="row">
            <div class="footer-content">
                <div class="col-md-6">
                    <h1>Jesal Chitala</h1>
                </div>

                <div class="col-md-6">
                    <ul class="links pull-right">
                        <li><a href="work.html">Work</a></li>
                        <li><a href="about.html">About</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="footer-content bottom-footer">
                <div class="col-md-6">
                    <p>Design by @Jesal Chitalia. All Rights Reserved.</p>
                </div>

                <div class="col-md-6">
                    <ul class="social pull-right">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>