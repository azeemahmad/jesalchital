<div class="header-top">
    <!--- headr start --->
    <div class="header">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.html" class="navbar-brand">Jesal Chitala</a>
                </div>
                <!-- Collection of nav links and other content for toggling -->
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.html">About Me</a></li>
                        <li><a href="work.html">Work</a></li>
                        <li><a href="process.html">Process</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--- headr end --->

    <!--- top-section start -->
    <div class="top-section">
        <div class="container">
            <div class="top-section-content">
                <h1>Hey, I am Jesal.</h1>
                <h1><strong>A Product Designer.</strong></h1>
                <a href="#" class="btn btn-default">Let's Connect</a>
            </div>
        </div>
    </div>
    <!--- top-section end -->
</div>