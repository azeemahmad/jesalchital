<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Jesal Chitala</title>

    <!-- Bootstrap -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/responsive.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--- headr-top start --->
<div class="header-top">
    <!--- headr start --->
    <div class="header">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="{{url('/')}}" class="navbar-brand">Jesal Chitala</a>
                </div>
                <!-- Collection of nav links and other content for toggling -->
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{url('/')}}">About Me</a></li>
                        <li><a href="{{url('/work')}}">Work</a></li>
                        <li><a href="{{url('/process')}}">Process</a></li>
                        <li class="active"><a href="{{url('/contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--- headr end --->

    <!--- top-section start -->
    <div class="top-section">
        <div class="container">
            <div class="top-section-content">
                <h1><strong>Lets Connect.</strong></h1>
            </div>
        </div>
    </div>
    <!--- top-section end -->
</div>
<!--- headr-top end --->

<div class="contact-wrapper default-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="contact-content">
                    <p>For all Work Related inquiries and general questioning, pleaes get in touch by filling this short form. I will get back to you in 43 hours.</p>
                </div>

                <div class="contact-content">
                    <p>Phone: <a href="#">(+1)902.4019629</a></p>
                    <p>Skype: <a href="#">jesalc</a></p>
                </div>
            </div>

            <div class="col-md-6">
                <form class="form">
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" placeholder="Full Name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="inputEmail" placeholder="Email Address">
                    </div>

                    <div class="form-group">
                        <input type="tel" class="form-control" id="mobile" placeholder="Contact No">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="subject" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="5" id="comment" placeholder="Message" style="resize: none;"></textarea>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox">Send me a copy too</label>
                    </div>
                    <button type="submit" class="btn btn-default">Send</button>
                </form>
            </div>
        </div>
    </div>
</div>



<!--- Footer start --->
<footer class="footer footer-padding">
    <div class="container">
        <div class="row">
            <div class="footer-content">
                <div class="col-md-6">
                    <h1>Jesal Chitala</h1>
                </div>

                <div class="col-md-6">
                    <ul class="links pull-right">
                        <li><a href="{{url('/work')}}">Work</a></li>
                        <li><a href="{{url('/')}}">About</a></li>
                        <li><a href="{{url('/contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="footer-content bottom-footer">
                <div class="col-md-6">
                    <p>Design by @Jesal Chitalia. All Rights Reserved.</p>
                </div>

                <div class="col-md-6">
                    <ul class="social pull-right">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--- Footer end --->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<scrip src="js/jquery.1.11.1.js"></scrip>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.js"></script>
<script src="js/main.js"></script>
</body>
</html>