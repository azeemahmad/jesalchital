<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Jesal Chitala</title>

    <!-- Bootstrap -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/responsive.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--- headr-top start --->
<div class="header-top">
    <!--- headr start --->
    <div class="header">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="{{url('/')}}" class="navbar-brand">Jesal Chitala</a>
                </div>
                <!-- Collection of nav links and other content for toggling -->
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="{{url('/')}}">About Me</a></li>
                        <li><a href="{{url('/work')}}">Work</a></li>
                        <li><a href="{{url('/process')}}">Process</a></li>
                        <li><a href="{{url('/contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--- headr end --->

    <!--- top-section start -->
    <div class="top-section">
        <div class="container">
            <div class="top-section-content">
                <h1>Hey, I am Jesal.</h1>
                <h1><strong>A Product Designer.</strong></h1>
                <a href="#" class="btn btn-default">Let's Connect</a>
            </div>
        </div>
    </div>
    <!--- top-section end -->
</div>
<!--- headr-top end --->

<!--- second-section start --->
<div class="second-section default-padding">
    <div class="container">
        <h1 class="main-heading">some of my works</h1>

        <div class="row">
            <div class="col-md-4">
                <div class="img-box">
                    <img src="http://placehold.it/400x400" class="img-responsive">
                </div>
            </div>

            <div class="col-md-8">
                <div class="img-box">
                    <img src="http://placehold.it/830x400" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 nopadding">
                <div class="col-md-12">
                    <div class="img-box">
                        <img src="http://placehold.it/600x870" class="img-responsive">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="img-box">
                        <img src="http://placehold.it/600x330" class="img-responsive">
                    </div>
                </div>
            </div>


            <div class="col-md-6 nopadding">
                <div class="col-md-12">
                    <div class="img-box">
                        <img src="images/placeholder-600x600.png" class="img-responsive">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="img-box">
                        <img src="images/placeholder-600x600.png" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="img-box">
                    <img src="http://placehold.it/830x400" class="img-responsive">
                </div>
            </div>

            <div class="col-md-4">
                <div class="img-box">
                    <img src="http://placehold.it/400x400" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</div>
<!--- second-section end --->

<!--- thingsdo start --->
<div class="thingsdo default-padding">
    <div class="container">
        <h1 class="main-heading">Things I DO</h1>

        <div class="row">
            <div class="col-md-4">
                <div class="thingsdo-box">
                    <img src="http://placehold.it/100x100" class="img-responsive">
                    <h3>UI Design</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo odit, consequuntur adipisci perspiciatis esse qui voluptas natus debitis et repudiandae</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="thingsdo-box">
                    <img src="http://placehold.it/100x100" class="img-responsive">
                    <h3>UI Design</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo odit, consequuntur adipisci perspiciatis esse qui voluptas natus debitis et repudiandae</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="thingsdo-box">
                    <img src="http://placehold.it/100x100" class="img-responsive">
                    <h3>UI Design</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo odit, consequuntur adipisci perspiciatis esse qui voluptas natus debitis et repudiandae</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--- thingsdo end --->

<!--- testimonial start --->
<div class="testimonial">
    <div class="testomonial-content">
        <img src="images/test-bg.jpg" class="img-responsive hidden-xs">
        <img src="http://placehold.it/768x1080" class="img-responsive visible-xs">

        <div class="overlay"></div>

        <div class="testimonial-text">
            <p>On a red carpet you really want to feel your best, so to work with a desginer who will shape a dress to your figure an dtake into into considertaion your opinions is really a blessing.</p>

            <div class="line"></div>

            <h3>Dhruv Jani <br> <small>Full Stack Devloper</small></h3>
        </div>
    </div>
</div>
<!--- testimonial end --->

<!--- about-me start --->
<div class="about-me default-padding">
    <div class="container">
        <div class="row aboutme-content">
            <div class="col-md-3">
                <h2>About me</h2>
            </div>
            <div class="col-md-9">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque a nemo ad ratione molestias cupiditate modi id, enim corrupti quaerat nulla, eos numquam optio recusandae aliquid iure debitis illo dolorum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos eaque nostrum ut natus dolores excepturi, praesentium ratione eligendi. Itaque iste odit voluptatibus mollitia facilis voluptatum tempora cupiditate incidunt et aperiam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro iusto voluptate, iste esse nam.</p>
            </div>
        </div>

        <div class="row aboutme-content">
            <div class="col-md-3">
                <h2>Experiance</h2>
            </div>
            <div class="col-md-9">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque a nemo ad ratione molestias cupiditate modi id, enim corrupti quaerat nulla, eos numquam optio recusandae aliquid iure debitis illo dolorum. </p>

                <p>Eos eaque nostrum ut natus dolores excepturi, praesentium ratione eligendi. Itaque iste odit voluptatibus mollitia facilis voluptatum tempora cupiditate incidunt et aperiam. </p>
            </div>
        </div>

        <div class="row aboutme-content">
            <div class="col-md-3">
                <h2>Resume</h2>
            </div>
            <div class="col-md-9">
                <a href="#" class="btn btn-default">View Resume</a>
                <a href="#" class="btn btn-default linkdin-btn"><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
</div>
<!--- about-me end --->

<!--- Footer start --->
<footer class="footer footer-padding">
    <div class="container">
        <div class="row">
            <div class="footer-content">
                <div class="col-md-6">
                    <h1>Jesal Chitala</h1>
                </div>

                <div class="col-md-6">
                    <ul class="links pull-right">

                        <li><a href="{{url('/work')}}">Work</a></li>
                        <li><a href="{{url('/')}}">About</a></li>
                        <li><a href="{{url('/contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="footer-content bottom-footer">
                <div class="col-md-6">
                    <p>Design by @Jesal Chitalia. All Rights Reserved.</p>
                </div>

                <div class="col-md-6">
                    <ul class="social pull-right">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--- Footer end --->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<scrip src="js/jquery.1.11.1.js"></scrip>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.js"></script>
<script src="js/main.js"></script>
</body>
</html>