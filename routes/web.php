<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@about')->name('about');
Route::get('/work', 'HomeController@work')->name('work');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/process', 'HomeController@process')->name('process');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
