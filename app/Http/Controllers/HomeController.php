<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function work()
    {
        return view('work');
    }
    public function contact()
    {
        return view('contact');
    }
    public function process()
    {
        return view('process');
    }
    public function about()
    {
        return view('welcome');
    }
}
